const ReturnHandler = require('../utils/ReturnHandler')
const Validator = require('../utils/Validator')

class XyzController {
    static async translate(letters, size, direction) {
        try {

            size = parseInt(size)
            letters = letters.toLowerCase()

            const errors = Validator.check([
                {
                    name: 'letters',
                    condition: new RegExp("^[x\y\z]+$").test(letters),
                    errorMessage: 'Invalid letter'
                },
                {
                    name: 'size',
                    condition: size % 2 === 1,
                    errorMessage: 'Invalid size, please use odd number'
                },
                {
                    name: 'size',
                    condition: size >= 3,
                    errorMessage: 'Invalid size, minimum size is 3'
                },
                {
                    name: 'direction',
                    condition: ['horizontal', 'vertical'].includes(direction),
                    errorMessage: 'Invalid direction, you can only choose horizontal and vertical'
                },
                {
                    name: 'letters',
                    condition: letters,
                    errorMessage: 'Missing parameter, letters is required'
                },
                {
                    name: 'size',
                    condition: size,
                    errorMessage: 'Missing parameter, size is required'
                },
                {
                    name: 'direction',
                    condition: direction,
                    errorMessage: 'Missing parameter, direction is required'
                }
            ])

            let code = 200
            let data = []
            let message = 'succesfully translated letters to ascii art'

            const collection = []

            if(errors.length === 0) {
                letters.split('').forEach((item) => {
                    let row = []
                    for(let x = 1; x <= size; x++) {
                        let line = ''
                        for(let y = 1; y <= size; y++) {
                            if(item === 'x') {
                                if(x === y || y === (size+1)-x) line += 'o'
                                else line += ' '
                            } else if (item === 'y') {
                                const middle = Math.ceil(size/2)
                                if(
                                    (x === y && y <= middle) || (y === middle && x >= middle) || (x + y === size + 1 && x < middle)
                                ) line += 'o'
                                else line += ' '
                            } else if (item === 'z') {
                                if(x === 1 || x === size || x + y === size + 1) line += 'o'
                                else line += ' '
                            }
                        }
                        row.push(line)
                    }
                    collection.push(row)
                })

                if(direction === 'vertical') {
                    for(let x = 0; x < collection.length; x++) {
                        for(let y = 0; y < size ; y++) {
                            console.log(collection[x][y])  
                        }
                        console.log('\r\n')
                    }
                } else if(direction === 'horizontal') {
                    for(let y = 0; y < size; y++) {
                        let line = ''
                        for(let x = 0; x < collection.length; x++) {
                            line += `${collection[x][y]} `
                        }
                        console.log(line)
                    }
                }
                data = collection
            } else {
                code = 400
                message = 'Validation error, please check response'
            }            

            return ReturnHandler.success(code, { data, errors }, message)
        } catch(error) {
            return ReturnHandler.error(error)
        }
    }
}

module.exports = XyzController