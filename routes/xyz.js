const express = require('express')
const router = express.Router()
const XyzController = require('../controllers/XyzController')  

router.route('/')
    .get(async (req, res) => {
        const { letters, size, direction } = req.query
    
        const response = await XyzController.translate(letters, size, direction)

        const { code, data, message } = response

        res.status(code).json({ data, message })
    })

module.exports = router