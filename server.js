const express = require('express')
const app = express()
const cors = require('cors')
const logger = require('node-color-log')

const PORT = 3051
const ROUTES = require('./utils/RouterMiddleware')

app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())

ROUTES.init(app, 'routes')

app.listen(PORT, () => {
    logger.info(`Listening on http://localhost:${PORT}`)
})

