class ReturnHandler {
  
    static getError(err){
       let message = 'We\'ve encountered some issues as of the moment. Please try again later. Thank you!';
       if(err){
           if(typeof err.errors !== 'undefined'){
               let error = err.errors[0]
               message =  err.errors.map(e => e.message).toString()
           }
           else if(typeof err.message !== 'undefined'){
               message = err.message
           }
       }

       return message;
   }

   static success(code, data, message){
       return JSON.parse(JSON.stringify({
           'code': code,
           'data': data,
           'message': message,
       }))
   }

   static error(err){
       return JSON.parse(JSON.stringify({
           'code': 400,
           'data': [],
           'message': this.getError(err),
           'error':err
       }))
   }
}

module.exports = ReturnHandler