const fs = require('fs')
const logger = require('node-color-log')
const path = require('path')

const API_CONTEXT = 'api'
const API_VERSION = 'v1'

class RouterMiddleware {
    static init(app, folderName) {
        logger.info('Initializing endpoint...')

        app.get(`/${API_CONTEXT}/${API_VERSION}/`, (req, res) => {
            res.status(200).json({ data: { ok: true }, message: 'API is up and running' })
        })
        
        fs.readdirSync(folderName).forEach(function (file) {
            var fullName = path.join(folderName, file)

            if (file.toLowerCase().indexOf('.js')) {

                let endpoint = `/${API_CONTEXT}/${API_VERSION}/${file.replace('.js', '')}`

                app.use(endpoint, (req, res, next) => { next () }, require('../' + fullName))
                logger.debug(endpoint)
            }
        });
    }

}


module.exports = RouterMiddleware;