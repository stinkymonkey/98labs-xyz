class Validator {
    static check(variables = []) {   
        let errors = []

        variables.forEach((item) => {
            const { name, condition, errorMessage } = item

            if(!condition) {
                errors.push({
                    name,
                    errorMessage
                })
            }
        })

        return errors
    }
}

module.exports = Validator